<?php

class TaggyStringUsageTest extends TestCase
{
    protected $lesson;

    public function setup()
    {
        parent::setUp();

        foreach (['PHP', 'Laravel', 'Testing', 'Redis', 'Postgres'] as $tag) {
            TagStub::create([
                'name' => $tag,
                'slug' => str_slug($tag),
                'count' => 0
            ]);
        }

        $this->lesson = LessonStub::create([
            'title' => 'A lesson title'
        ]);
    }

    /** @test */
    function can_tag_lesson()
    {
        $this->lesson->tag(['laravel', 'php']);

        $this->assertCount(2, $this->lesson->tags);

        foreach (['Laravel', 'PHP'] as $tag) {
            $this->assertContains($tag, $this->lesson->tags->pluck('name'));
        }
    }
}
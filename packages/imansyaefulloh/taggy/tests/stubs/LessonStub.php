<?php

use Illuminate\Database\Eloquent\Model;
use Imansyaefulloh\Taggy\TaggableTrait;
use Imansyaefulloh\Taggy\Scopes\TagUsedScopesTrait;
use Imansyaefulloh\Taggy\Scopes\TaggableScopesTrait;

class LessonStub extends Model
{
    use TaggableTrait, TaggableScopesTrait, TagUsedScopesTrait;

    protected $connection = 'testbench';

    public $table = 'lessons';


}

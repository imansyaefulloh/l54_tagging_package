<?php

namespace Imansyaefulloh\Taggy\Scopes;

trait TagUsedScopesTrait
{
    // used greater than or equal
    public function scopeUsedGte($query, $count)
    {
        return $query->where('count', '>=', $count);
    }

    // used greater than
    public function scopeUsedGt($query, $count)
    {
        return $query->where('count', '>', $count);
    }

    // used less than or equal
    public function scopeUsedLte($query, $count)
    {
        return $query->where('count', '<=', $count);
    }

    // used less than
    public function scopeUsedLt($query, $count)
    {
        return $query->where('count', '<', $count);
    }
}
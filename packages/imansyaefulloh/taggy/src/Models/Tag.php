<?php

namespace Imansyaefulloh\Taggy\Models;

use Illuminate\Database\Eloquent\Model;
use Imansyaefulloh\Taggy\Scopes\TagUsedScopesTrait;

class Tag extends Model
{
    use TagUsedScopesTrait;   
}
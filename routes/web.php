<?php

use App\Topic;
use App\Lesson;

use Imansyaefulloh\Taggy\Models\Tag;

Route::get('/', function () {

    // $lesson = new Lesson();
    // $lesson->title = 'Title 1';
    // $lesson->title = 'Title 2';
    // $lesson->save();
    
    // $lesson = Lesson::find(1);
    // $lessons = Lesson::withAnyTag(['khaki']);
    // $lessons = Lesson::withAllTags(['magenta', 'peachpuff']);

    $tags = Tag::usedGte(1)->get();

    dump($tags);

    // $tag = Tag::where('slug', 'khaki')->first();
    // $tags = Tag::whereIn('slug', ['khaki', 'magenta', 'peachpuff'])->get();

    // $lesson->tag(['khaki', 'magenta']);
    // $lesson->tag(['khaki', 'magenta']);
    // $lesson->retag($tag);
    // $lesson->retag($tags);
    // $lesson->untag();
});
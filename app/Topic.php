<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Imansyaefulloh\Taggy\TaggableTrait;
use Imansyaefulloh\Taggy\Scopes\TagUsedScopesTrait;
use Imansyaefulloh\Taggy\Scopes\TaggableScopesTrait;

class Topic extends Model
{
    use TaggableTrait, TaggableScopesTrait, TagUsedScopesTrait;
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Imansyaefulloh\Taggy\TaggableTrait;
use Imansyaefulloh\Taggy\Scopes\TagUsedScopesTrait;
use Imansyaefulloh\Taggy\Scopes\TaggableScopesTrait;

class Lesson extends Model
{
    use TaggableTrait, TaggableScopesTrait, TagUsedScopesTrait;
}
